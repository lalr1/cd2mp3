cd2mp3 v0.4
Copyright (C) 2005, 2015 Finn Lawler.
Released under the GNU General Public License.


cd2mp3
------

cd2mp3 is a Perl script that helps to turn audio CDs into MP3
compressed audio files.  It uses the `cdda2wav' tool to grab (`rip')
audio tracks from a CD and the `LAME' encoder to turn the resulting
files into MP3 files.  Its chief purpose is to allow the user to
specify ID3 tags (pieces of text embedded in MP3 files that give
information about the piece of music, such as title and artist)
concisely, and to take (some of) the tedium out of running the other
programs.

The ID3 tags to be embedded, and their values, are specified by an
input file, the syntax of which is described below.


Running cd2mp3
--------------

The program accepts several options, including the standard GNU
options of `--help' and `--version'.  As usual, there are short
(one-character) forms that can be used instead, and long options can
be abbreviated provided that the abbreviation is unique across long
options.  The non-standard options are as follows:

  -q, --quiet

      Pass `--quiet' as an additional argument to cdda2wav and LAME.
      This doesn't suppress output from cd2mp3 itself.


  -s, --silent

      As --quiet, but also suppress the printing of each cdda2wav and
      LAME command line before it is executed.  Only warnings and
      errors are printed to the terminal.


  -a, --all

      Process all tracks on the CD.  The default action is to skip
      tracks that have no ID3 tags defined for them, unless they were
      specified with a `--track' argument.  Tracks with no defined
      tags go, if encoded, into files named `<prefix>N.mp3', where N
      is the number of the track, and <prefix> is given by the
      `--prefix' option (see below).  You can thus rip and encode an
      entire CD with `cd2mp3 -a', if you don't want to bother with
      tags and filenames.  Note that if you supply both `--all' and an
      input file, the track-number upper bound is taken from the file,
      not the CD, which is probably not what you want.

      To find out how many tracks are on the CD, cd2mp3 runs cdda2wav
      using the -J and `-v toc' options, and looks for a line
      beginning with `Tracks:NN', where NN is a decimal number.  It
      uses this number as the upper bound on track numbers.  This may
      be a somewhat flaky approach, so use a `--track' option if it
      doesn't work.  cd2mp3 only needs to know this number if `--all'
      is given but no input file is.

      If `--no-rip' is given, then `--all' does what you'd expect --
      encodes all of the files named `trackN.wav' or similar, even if
      they have no tags defined.

      The functioning of `--all' and its interaction with other
      options might seem messy; it certainly does to me.  Here is a
      summary:

        - `--track' processes all of the tracks given, regardless of
          whether they have tags defined.

	- `--all' processes as many tracks as it can find, either on
          the CD or in the current directory (if `--no-rip' is given).

	- In the absence of both of the above, all tracks will be
          processed that have tags defined in the input file.  If
          `--no-rip' is also specified then non-existent WAV files are
          silently skipped.

      In short, you must specify at least one of: input file,
      `--track' and `--all'.  `--all' has no effect in the presence of
      `--track'.


  -n, --dry-run

      Run as normal, but do not actually run external commands
      (cdda2wav and LAME).  Most useful for debugging the script and
      experimenting with its options, since like `make -n' this will
      print the command lines that would be executed (unless
      `--silent' is also given).


  -d, --dump

      Process input files as described below, but, instead of running
      external commands, print out the contents of the internal data
      structures representing the expanded ID3 tag values of the files
      to be processed, exactly as they would be passed to LAME.  Most
      useful for checking that an input file will do what you expect.
      The output that this option produces is valid as input to
      cd2mp3, though that fact won't often come in handy.


  -e, --exit-on-error

      Exit if cdda2wav returns error code 11, meaning it encountered
      errors on the CD.  The default action in such cases is to
      continue with the next track.  In all other cases external
      command failure will cause cd2mp3 to exit with the error code
      returned.

      If an external command can be determined to have died on a
      signal SIG, then that fact is noted in a message on stderr, and
      cd2mp3 exits with status SIG+128.


  -N, --no-rip

      Do not run cdda2wav to rip audio files, but instead expect to
      find them in the current directory, with names of the form
      `trackN.wav' or similar (see `--prefix' below), where N is the
      track number.  If `trackN.wav' does not exist, for given N, then
      LAME is not invoked, and cd2mp3 proceeds (silently) to the next
      track.  That `given N' will be taken from a `--track' option or
      an input file.

      If `--no-rip' is used but no tracks are specified on the command
      line then cd2mp3 will generate a list of tracks to be processed
      from the `N'-parts of filenames in the current directory that
      look like `trackN.wav'.  That is, if `--prefix' is `track' (the
      default), and if the current directory contains the files
      `track1.wav', `track7.wav' and `track10.wav', then only tracks
      1, 7 and 10 will be processed.

      This option will be most useful if you want to use cd2mp3 to
      take the work out of specifying ID3 tags and values, but need to
      do some processing of the ripped audio before encoding it, e.g.:

        $ cdda2wav -D /dev/cdrom -xH -B -O wav --track 1+12 trk
	$ for t in trk*.wav; do \
	>  frobnicate --blorkify=yes --znorb-rate=3.141 $t; \
	> done
	$ cd2mp3 --no-rip --prefix=trk tags.file

      The above will rip the first 12 tracks from the CD in
      /dev/cdrom, storing them as CD-quality WAV files in `trkN.wav'.
      After running the `frobnicate' program on them, it will encode
      the `trkN.wav' files using the tag specs in `tags.file'.  On the
      other hand, if you're doing this frequently, you might want to
      add an option to cd2mp3 that will allow an arbitrary external
      command to be run on the WAV file before it is passed to LAME.
      This would be quite easy.

      Note that, although cdda2wav and LAME can handle formats other
      than WAV, cd2mp3 always assumes that the extension of a
      temporary file is `.wav' (which will cause LAME to assume the
      obvious -- we also pass `-O wav' to cdda2wav, just to be on the
      safe side).  This is something I'd like to change, and soon.

  -E, --no-encode

      Don't run either cdda2wav or LAME.  Instead run id3tag on the
      MP3 files that LAME would have written, to set the contents of
      the tags according to the input file.  The files are looked for
      in the output directory, i.e. either `.' or the directory
      specified with the `-O' option (see below), under the name given
      by the track's `file' attribute in the input file or
      `<prefix>N.mp3' if that is not set.  Missing MP3 files are
      silently skipped.

      This might be useful if e.g. you make a mistake in the input
      file -- you can correct it and run cd2mp3 with this option in
      order to change the tags of the MP3 files just encoded without
      having to rip or encode them again.


  -C, --cdda2wav-opts STRING

      Pass the whitespace-separated arguments in STRING to cdda2wav
      instead of the defaults.  This does not override `--device' or
      `--quiet' options.  The defaults give CD-quality recording in
      WAV format, suppress the generation of .inf and CDDB files by
      cdda2wav, and restrict informational output (the `-v summary'
      option).


  -L, --lame-opts STRING

      Pass the options in STRING to LAME instead of the defaults.  The
      `--quiet' option is still respected.  The default LAME options
      give high quality encoding (the `standard' preset; variable
      bit-rate joint stereo) and suppress the histogram feature.


  -D, --device

      This option specifies the device containing the CD to be
      processed, and whose name will be passed to cdda2wav.  The
      device name can be either the path to a device file in /dev or a
      SCSI device identifier of the form <bus>,<device ID>,<LUN> (see
      the cdda2wav(1) man page for details).

      The interface (`-I') argument to cdda2wav is inferred from the
      device name -- `generic_scsi' for names matching
      /^\d+,\d+,\d+$/, `cooked_ioctl' for everything else.

  -t, --track TRACK-SPEC

      Operate on the tracks in TRACK-SPEC only.  The argument
      TRACK-SPEC should be a comma-separated list of numbers and/or
      number-ranges, such as `1,3,5-10'.  Ranges are inclusive.  Since
      they are implemented with the Perl `..' operator, they share its
      semantics; the case of `N-N' simply means N, and `N-M' where N>M
      has no effect.


  -O, --output-dir DIRECTORY

      Put created MP3 files into DIRECTORY.  The default is `.' (the
      current directory).


  -p, --prefix STRING

      Make temporary WAV files have prefix STRING, instead of the
      default `track'.  Such files are of the form <prefix>N.PID.wav,
      where PID is the process ID of the creating instance of cd2mp3.
      This option also changes the prefix of MP3 files with no `file'
      attribute defined -- they are called `<prefix>N.mp3'.

      If `--no-rip' is specified, the filenames passed to LAME are of
      the form <prefix><trackno>.wav; see above for details.
      Temporary files (those created by cd2mp3-invoked instances of
      cdda2wav) are removed after encoding; files found with
      `--no-rip' are, of course, not.


Temporary WAV files are stored in the directory given by the
environment variable TMPDIR, or TMP if that is not set, or in /tmp if
neither is set.  TMPDIR and TMP are ignored if `--no-rip' is specified
-- the files to pass to LAME are always looked for in the current
directory.  In theory, you could use `--prefix' to get around this, by
specifying a prefix with directory components; this might work, but
it's messy.  For example, if you used `--no-rip --prefix=/tmp/trk'
then cd2mp3 would look for files called `/tmp/trkN.wav', but if some
tracks have no `file' attribute defined then cd2mp3 will try to put
them into /path/to/output-dir/tmp/trkN.mp3, which is probably not what
you want.

A bare argument is treated as the name of an input file.  Perl-style
`pipe' file names are valid; use `command|' to specify the output of
`command' as input, or `-' to use standard input.  This means that
you'll have trouble using an input file whose name begins with a
redirection operator, but chances are you won't have many of those.

Some of the options may seem rather complicated.  In most cases this
will merely be the fault of my explanations.  cd2mp3 should do the
Right Thing most of the time, and for most applications a simple input
file, and option-less invocation, will do the trick.  Experiment with
the options using the `--dry-run' switch to see what effects they
have.  If there's something you don't like about cd2mp3, change it and
send me a diff.


Input Files
-----------

ID3 tags and values to be passed to LAME are described by the input
file given to cd2mp3.  For CS types, here's an EBNF grammar, in which
`/foo/' means a string that matches the Perl regular expression `foo':

 <line>		::=  /\s*/ { <comment> | <track-spec> | <tag-spec>
			    | <resetm> } /\s*/ '\n'
 <comment>	::=  '#' /[^\n]*/
 <track-spec>	::=  { { <number> | <range> } /\s+/ }*
		      { <number> | <range> }
 <range>	::=  <number> '-' <number>
 <number>	::=  /\d+/
 <tag-spec>	::=  <alpha> /\s*/ { '=' | '<' | '>' } /\s*/ /[^\n]*/
 <resetm>	::=  'resetm'
 <alpha>	::=  /[[:alpha:]]+/   (C locale equivalent: [a-zA-Z])

Lines matching /^\s*$/ are ignored.  In English, the above means that
each line of input can be empty except for whitespace, or

  a comment, which consists of optional whitespace, then `#', then
  anything, until the end of the line, or

  a track specification, which consists of at least one number or
  range, more than one being separated by whitespace, or

  the string `resetm', or

  a tag specification, which is of the form <tag> [=<>] <value>.

Comments are of course discarded.  As the input is read, a list is
kept of the current tracks, which starts out empty and is set by
<track-spec> lines.  So after reading this:

  1 2 7-10

the current tracks are 1, 2, 7, 8, 9 and 10.  This list is used to
control which tracks have tags defined by <tag-spec> lines.  This

  tag = value

sets the tag `tag' of the current tracks to `value' (leading and
trailing whitespace is removed from the value); this, however:

  tag > value

*appends* `value' to the value of `tag' (for each current track), and,
as you might guess, this:

  tag < value

prepends `value' to `tag'.  Backslashes (`\') are removed from the
beginning and end of the value of a tag-spec, if they are present, so
to preserve leading and/or trailing space just protect it with `\'.
Backslashes are preserved within the string, though.

But that's not all.  There are two substitutions available within tag
values.  The first is `%t', which is replaced everywhere with the
current track number.  So for an album with 12 tracks, the following
will set the `track' tag properly for each:

  1-12
  track = %t

To include a literal `%', use `%%'.  The second substitution is `%m',
which is replaced with the `movement number'.  This works as follows.
A `resetm' line indicates that each of the current tracks is to be
considered the start of a piece, and that the movement numbers of
tracks after each `resetm' track are to increase from 1.  In effect,
this is almost like the track number, except it can be reset to 1 with
`resetm'.

A special feature that only applies to a `track' tag is that if after
%-substitution it looks like `N+M' or `N-M' where N and M are natural
numbers then that expression is evaluated to get the final value of
the tag.  This is useful for multi-CD albums or multi-album CDs.

Here is a real example of a pair of satisfyingly concise input files
for a 2-CD album:

=== brand1 ===

1-13
artist = J. S. Bach
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
year = 1987
title = Concerto no. \
album = Brandenburg Concertos

1 5 8 11
resetm

1-4
title > 1 in F - %m: \
file = bwv1046_%m.mp3
5-7
title > 2 in F - %m: \
file = bwv1047_%m.mp3
8-10
title > 3 in G - %m: \
file = bwv1048_%m.mp3
11-13
title > 4 in G - %m: \
file = bwv1049_%m.mp3

1 3 5 8 10 11
title > Allegro
2 9
title > Adagio
4
title > Menuetto, Trio, Polacca, Trio
6 12 
title > Andante
7
title > Allegro assai
13
title > Presto

=== end ===


=== brand2 ===

1-9
artist = J. S. Bach
year = 1987
1-6
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
title = Concerto no. \
album = Brandenburg Concertos
track = %t + 13

7-9
comment = Yehudi Menuhin, Alberto Lysy, Hu Kun, Camerata Lysy Gstaad
title = Concerto in D for 3 violins - %m: \
track = %t - 6

1 4 7
resetm

1-3
title > 5 in D - %m: \
file = bwv1050_%m.mp3
4-6
title > 6 in Bb - %m: \
file = bwv1051_%m.mp3
7-9
file = bwv1064_%m.mp3

1 3 4 6 7 9
title > Allegro
2
title > Affetuoso
5
title > Adagio ma non tanto
8
title > Adagio

=== end ===

Here is the output of `cd2mp3 -d -t 1-6 brand1':

=== begin ===

1
artist = J. S. Bach
title = Concerto no. 1 in F - 1: Allegro
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
year = 1987
track = 1
album = Brandenburg Concertos
file = bwv1046_1.mp3
2
title = Concerto no. 1 in F - 2: Adagio
artist = J. S. Bach
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
year = 1987
track = 2
album = Brandenburg Concertos
file = bwv1046_2.mp3
3
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
title = Concerto no. 1 in F - 3: Allegro
artist = J. S. Bach
file = bwv1046_3.mp3
album = Brandenburg Concertos
track = 3
year = 1987
4
year = 1987
track = 4
file = bwv1046_4.mp3
album = Brandenburg Concertos
title = Concerto no. 1 in F - 4: Menuetto, Trio, Polacca, Trio
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
artist = J. S. Bach
5
title = Concerto no. 2 in F - 1: Allegro
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
artist = J. S. Bach
file = bwv1047_1.mp3
album = Brandenburg Concertos
year = 1987
track = 5
6
title = Concerto no. 2 in F - 2: Andante
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
artist = J. S. Bach
album = Brandenburg Concertos
file = bwv1047_2.mp3
track = 6
year = 1987

=== end ===

And `cd2mp3 -d -t 6,7 brand2':

=== begin ===
6
year = 1987
title = Concerto no. 6 in Bb - 3: Allegro
track = 19
album = Brandenburg Concertos
artist = J. S. Bach
comment = Academy of St. Martin-in-the-Fields, Sir Neville Marriner
file = bwv1051_3.mp3
7
artist = J. S. Bach
file = bwv1064_1.mp3
comment = Yehudi Menuhin, Alberto Lysy, Hu Kun, Camerata Lysy Gstaad
track = 1
year = 1987
title = Concerto in D for 3 violins - 1: Allegro
=== end ===


ID3 tags
--------

You can use any name (matching /[[:alpha:]]+/) as a tag in an input
file, but only valid ID3 tags are passed to LAME.  They are as
follows:

  - title
  - artist
  - album
  - year
  - comment
  - track
  - genre

cd2mp3 treats everything in a case-sensitive manner, so the tags
should be spelt *exactly* as above.  As far as I know, `year' should
contain a 4-digit positive number (rather than just any string) and is
used for the year of issue of the recording, not the year of
composition.  The `genre' tag should also, I believe, have a numeric
value, used to select a genre name from a standard list (use `lame
--genre-list' to see it).  cd2mp3 does not verify that these
constraints are satisfied.  See the lame(1) man page for details.

cd2mp3 understands one other tag, which is `file'.  This is used to
set the name of the output (MP3) file for the current tracks.  As
witnessed in the above example, the `%t' and `%m' substitutions are
made in `file' tag values.

